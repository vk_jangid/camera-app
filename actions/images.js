const storingImages = images => ({
  type: 'STORE_IMAGE',
  images,
});

const addImage = image => ({
  type: 'ADD_IMAGE',
  image,
});

export {storingImages, addImage};
