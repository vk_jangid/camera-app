import React from 'react';
import CameraRoll from '@react-native-community/cameraroll';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import {FlatList, Image, StyleSheet, View, Dimensions} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const ShowImage = ({item, navigation}) => {
  return (
    <TouchableOpacity
      style={{
        width: Dimensions.get('screen').width * 0.33,
        height: 130,
        margin: 0.7,
      }}
      onPress={() =>
        navigation.navigate('Preview', {
          pathOfImage: item.node.image.uri,
        })
      }>
      <Image source={{uri: item.node.image.uri}} style={styles.image} />
    </TouchableOpacity>
  );
};

class appImages extends React.Component {
  state = {
    appImages: [],
    lastImage: '',
  };

  fetchPhotos = () => {
    CameraRoll.getPhotos({
      first: 20,
      after: this.state.lastImage,
      assetType: 'Photos',
      groupName: 'DCIM',
    })
      .then(res => {
        this.setState(prevState => ({
          appImages: prevState.appImages.concat(res.edges),
          lastImage: res.page_info.end_cursor,
        }));
      })
      .catch(err => console.log('err', err));
  };

  componentDidMount() {
    this.fetchPhotos();
  }

  render() {
    return (
      <View>
        <FlatList
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
          data={this.state.appImages}
          renderItem={({item, index}) => {
            console.log(item);
            return <ShowImage item={item} navigation={this.props.navigation} />;
          }}
          onEndReachedThreshold={0.5}
          onEndReached={() => this.state.lastImage && this.fetchPhotos()}
        />
      </View>
    );
  }
}

class cameraImages extends React.Component {
  state = {
    cameraImages: [],
    lastImage: '',
  };

  fetchPhotos = () => {
    CameraRoll.getPhotos({
      first: 20,
      after: this.state.lastImage,
      assetType: 'Photos',
      groupName: 'Camera',
    }).then(res => {
      this.setState(prevState => ({
        cameraImages: prevState.cameraImages.concat(res.edges),
        lastImage: res.page_info.end_cursor,
      }));
    });
  };

  componentDidMount() {
    this.fetchPhotos();
  }

  render() {
    return (
      <View>
        <FlatList
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
          data={this.state.cameraImages}
          renderItem={({item, index}) => {
            return <ShowImage item={item} navigation={this.props.navigation} />;
          }}
          onEndReachedThreshold={0.5}
          onEndReached={() => this.state.lastImage && this.fetchPhotos()}
        />
      </View>
    );
  }
}

const Tab = createMaterialTopTabNavigator();

class ImagesScreen extends React.Component {
  state = {
    imagesuri: [],
  };

  render() {
    return (
      <Tab.Navigator>
        <Tab.Screen name="App Image" component={appImages} />
        <Tab.Screen name="Camera Image" component={cameraImages} />
      </Tab.Navigator>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
  },
});

export default ImagesScreen;
