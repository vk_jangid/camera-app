import React from 'react';
import {View, Image, Dimensions} from 'react-native';

const PreviewImage = ({route}) => {
  const {pathOfImage} = route.params;
  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <Image
        source={{uri: pathOfImage}}
        style={{height: '100%'}}
        resizeMode="contain"
      />
    </View>
  );
};

export default PreviewImage;
