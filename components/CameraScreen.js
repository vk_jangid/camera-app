import React from 'react';
import {RNCamera} from 'react-native-camera';
import CameraRoll from '@react-native-community/cameraroll';
import PushNotification from 'react-native-push-notification';
import Share from 'react-native-share';
import ImageMarker from 'react-native-image-marker';

import Icon from 'react-native-vector-icons/AntDesign';

import {
  View,
  TouchableOpacity,
  PermissionsAndroid,
  Image,
  StyleSheet,
  Linking,
} from 'react-native';

class CameraScreen extends React.Component {
  constructor() {
    super();
    PushNotification.configure({
      onRegister: function(token) {
        console.log('token', token);
      },
      onNotification: function(notification) {
        // Linking.openURL(notification.location);
        const options = {url: notification.filePath};
        Share.open(options)
          .then(res => console.log(res))
          .catch(err => console.log(err));
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: Platform.OS === 'ios',
    });
  }

  state = {
    firstImageuri: '',
    rearCamera: true,
  };

  componentDidMount() {
    CameraRoll.getPhotos({first: 1, assetType: 'Photos', groupName: 'DCIM'})
      .then(res => {
        this.setState({
          firstImageuri: res.edges[0].node.image.filename,
        });
      })
      .catch(err => console.log('[ERROR] No photos found'));
  }

  render() {
    const cameraType = this.state.rearCamera ? 'back' : 'front';
    return (
      <View style={styles.container}>
        <RNCamera
          playSoundOnCapture={true}
          style={styles.preview}
          type={RNCamera.Constants.Type[cameraType]}
          // flashMode={RNCamera.Constants.FlashMode.on}
          ref={cam => (this.camera = cam)}
          onCameraReady={() => this.checkAndroidPermission()}
        />
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginVertical: 50,
            paddingHorizontal: 50,
          }}>
          {this.state.firstImageuri ? (
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Images')}>
              <Image
                source={{
                  uri: `file:///storage/emulated/0/DCIM/${
                    this.state.firstImageuri
                  }`,
                }}
                style={styles.iconImage}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={{
                padding: 30,
                borderWidth: 1,
                borderStyle: 'solid',
                borderColor: '#fff',
                borderRadius: 50,
                backgroundColor: 'gray',
              }}
              onPress={() => this.props.navigation.navigate('Images')}
            />
          )}
          <View style={styles.capture}>
            <TouchableOpacity
              onPress={() => this.takePicture(this.camera)}
              style={styles.captureBtn}
            />
          </View>
          <TouchableOpacity
            onPress={() =>
              this.setState(prevState => {
                return {rearCamera: !prevState.rearCamera};
              })
            }>
            <Icon name="sync" size={45} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  checkAndroidPermission = async () => {
    try {
      const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
      await PermissionsAndroid.request(permission);
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  };

  takePicture = async function(camera) {
    try {
      const options = {
        quality: 1,
        base64: true,
      };
      await this.checkAndroidPermission();
      const data = await camera.takePictureAsync(options);
      const watermark = await ImageMarker.markText({
        src: data.uri,
        text: 'Taken on Honor Play',
        position: 'bottomRight',
        color: '#6e6e6e',
        fontName: 'Arial-BoldItalicMT',
        fontSize: 44,
        scale: 1,
        quality: 100,
      });
      const path = await CameraRoll.save(watermark);
      const uri = watermark.split('/');
      PushNotification.localNotification({
        title: 'Picture clicked',
        message: `The file path for picture is file:///storage/emulated/0/DCIM/${
          uri[uri.length - 1]
        }`,
        filePath: data.uri,
        location: path,
      });
      this.setState({firstImageuri: uri[uri.length - 1]});
    } catch (err) {
      console.log('error', err);
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    borderWidth: 3,
    borderStyle: 'solid',
    borderColor: '#fff',
    borderRadius: 50,
    padding: 2,
    alignSelf: 'center',
  },
  captureBtn: {
    backgroundColor: 'white',
    width: 70,
    height: 70,
    borderRadius: 50,
  },
  iconImage: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 50,
  },
});

export default CameraScreen;
