import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {connect} from 'react-redux';

import CameraScreen from './components/CameraScreen';
import ImagesScreen from './components/ImagesScreen';
import login from './components/Login';
import PreviewImage from './components/PreviewImage';

const Stack = createStackNavigator();

const Routes = props => {
  const homeRoute = props.isLoggedIn ? 'Home' : 'Login';
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={homeRoute}>
        <Stack.Screen
          name="Home"
          component={CameraScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Images" component={ImagesScreen} />
        <Stack.Screen name="Login" component={login} />
        <Stack.Screen name="Preview" component={PreviewImage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = state => ({
  isLoggedIn: state.login.isLoggedIn,
});

export default connect(
  mapStateToProps,
  null,
)(Routes);
