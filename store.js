import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import {AsyncStorage} from 'react-native';

import rootReducer from './reducers';

const initialState = {};

const middleware = applyMiddleware(thunk);

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, initialState, middleware);

const persistedStore = persistStore(store);

export {store, persistedStore};
