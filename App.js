import React from 'react';
import {Provider, connect} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

import {View, ActivityIndicator} from 'react-native';

import {store, persistedStore} from './store';

import * as Sentry from '@sentry/react-native';
import Routes from './Routes';

Sentry.init({
  dsn:
    'https://0be9c3c4d7954d5d9becf28b36f513ae@o403466.ingest.sentry.io/5266133',
});

class App extends React.Component {
  renderLoading = () => {
    return (
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size={'large'} />
      </View>
    );
  };

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistedStore} loading={this.renderLoading()}>
          <Routes />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
