const inititalState = {
  isLoggedIn: false,
  userDetails: [],
};

export default (state = inititalState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        isLoggedIn: true,
        userDetails: action.userDetails,
      };
    default:
      return state;
  }
};
