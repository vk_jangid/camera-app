import {combineReducers} from 'redux';

import login from './login';
import images from './images';

export default combineReducers({login, images});
