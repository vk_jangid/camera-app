const initialState = {
  images: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'STORE_IMAGE':
      return {
        ...state,
        images: action.images,
      };
    case 'ADD_IMAGE':
      return {
        ...state,
        images: [action.image, ...state.images],
      };
    default:
      return state;
  }
};
